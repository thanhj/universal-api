FROM openjdk:8-jdk as builder
WORKDIR /app
ADD ./ /app
RUN ./gradlew --stacktrace clean test build

##############
FROM adoptopenjdk/openjdk8:alpine-jre as service
ENV SPRING_PROFILES_ACTIVE='production'
WORKDIR /app
COPY --from=builder /app/build/libs/universal-api*.jar /app/universal-api.jar

EXPOSE 8080
EXPOSE 8081

ENTRYPOINT java -jar -Dspring.profiles.active=$SPRING_PROFILES_ACTIVE /app/universal-api.jar